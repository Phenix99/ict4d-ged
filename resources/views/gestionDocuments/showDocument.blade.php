@extends('layout.index')
@section('appContent')

    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Listes des Documents</h4>
                        <h6 class="card-subtitle">Cliquez sur les boutons d'actions pour plus d'options</h6>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Intitule</th>
                                    <th>Description</th>
                                    <th>Categories</th>
                                    <th>Type</th>
                                    <th>Création</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Intitule</th>
                                    <th>Description</th>
                                    <th>Categories</th>
                                    <th>Type</th>
                                    <th>Création</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                                <tbody>


                                <tr>
                                    <td>PV ICT307 - 2018/2019</td>
                                    <td>Proccès verbal de ICT307 - 2018/2019</td>
                                    <td>PV 2018/2019</td>
                                    <td>PDF</td>
                                    <td>2012/04/09</td>
                                    <td><button class="btn btn-primary">Ouvr</button>&#160;<button class="btn btn-primary">Arch</button>&#160;<button class="btn btn-primary">Dépl</button>&#160;<button class="btn btn-primary">Supp</button>&#160;<button class="btn btn-primary">Téléch</button>&#160;<button class="btn btn-primary">Convert</button>&#160;<button class="btn btn-primary">Réviser</button></td>

                                </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    @endsection