@extends('layout.index')
@section('appContent')

    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">CORBEILLE</h4>
                        <h6 class="card-subtitle">Cliquez sur les boutons d'actions pour plus d'options</h6>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Documents</th>
                                    <th>Description</th>
                                    <th>Date de Surppression</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Documents</th>
                                    <th>Description</th>
                                     <th>Date de surppression</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                <tr>
                                    <td>Etudiants L3</td>
                                    <td>Liste des étudiants - ICT4D L3 2017/2018</td>
                                    <td>12/02/2019</td>
                                    <td><button class="btn btn-primary">Restore</button><!-- <button class="btn btn-primary">Destroy</button> -->
                                </tr>
                                <tr>
                                    <td>Etudiants L3</td>
                                    <td>Liste des étudiants - ICT4D L3 2017/2018</td>
                                    <td>12/02/2019</td>
                                    <td><button class="btn btn-primary">Restore</button><!-- <button class="btn btn-primary">Destroy</button> -->
                                </tr>
                                <tr>
                                    <td>Etudiants L3</td>
                                    <td>Liste des étudiants - ICT4D L3 2017/2018</td>
                                    <td>12/02/2019</td>
                                    <td><button class="btn btn-primary">Restore</button><!-- <button class="btn btn-primary">Destroy</button> -->
                                </tr>
                                <tr>
                                    <td>Etudiants L3</td>
                                    <td>Liste des étudiants - ICT4D L3 2017/2018</td>
                                    <td>12/02/2019</td>
                                    <td><button class="btn btn-primary">Restore</button><!-- <button class="btn btn-primary">Destroy</button> -->
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
@endsection